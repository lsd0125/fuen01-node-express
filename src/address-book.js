const express = require('express');
const db = require('./tedious_connect');
const moment = require('moment-timezone');

const router = express.Router();


router.get('/list/:page?', async (req, res)=>{
    res.locals.pageName = 'address-book-list';

    const perPage = 2;
    let page = parseInt(req.params.page) || 1;
    const output = {
        perPage,
        page,
        totalRows: 0,
        totalPages: 0,
        rows: []
    }

    const t_sql = "SELECT COUNT(1) num FROM address_book ";
    const t_result = await db.myExecSql(t_sql);
    output.totalRows = t_result.rows[0].num;
    output.totalPages = Math.ceil(output.totalRows/perPage);

    if(output.totalRows > 0){
        const sql = `SELECT * FROM address_book ORDER BY sid DESC
            OFFSET ${(page-1)*perPage} ROWS FETCH NEXT ${perPage} ROWS ONLY
        `;
        const result = await db.myExecSql(sql);

        const fm = "YYYY-MM-DD";
        result.rows.forEach(el=>{
            el.birthday = moment(el.birthday).format(fm);
        });
        output.rows = result.rows;
    }
    res.render('address-book/list', output);
    // res.json(output);
});

router.get('/', (req, res)=>{
    res.redirect(req.baseUrl + '/list');
});

module.exports = router;
