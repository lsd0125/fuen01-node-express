// 1. MS SQL Server Management Studio 登入後, 主機按右鍵, 選[安全性], 使用[SQL Server 及 windows 驗證模式]
// 2. SQL Server Configuration Manager 
// SQL Server 網路組態 > MSSQLSERVER 的通訊協定 > TCP/IP 按右鍵選內容 > 選[IP位址]分頁 > 啟用通訊埠
// 3. 重新開機

const sql = require('mssql');

const config = {
    user: 'sa',
    password: 'P@ssw0rd',
    // user: 'root',
    // password: 'root',
    server: '192.168.140.64', // You can use 'localhost\\instance' to connect to named instance
    database: 'TestDB',
}
 
const h = async () => {
    try {
        // make sure that any items are correctly URL encoded in the connection string
        await sql.connect(config)
        const result = await sql.query(`select * from address_book`)
        console.dir(result)
    } catch (err) {
        console.log(err);
    }
};
h();