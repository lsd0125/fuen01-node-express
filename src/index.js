const express = require('express');
express.shinder_data = 456;

const fs = require('fs');
const multer = require('multer');
const upload = multer({dest: 'tmp_uploads/'});
const upload2 = require('./upload-module');
const {v4: uuidv4} = require('uuid');
const session  = require('express-session');
const moment = require('moment-timezone');
const db = require('./tedious_connect');
const cors = require('cors');

const app = express();

app.set('view engine', 'ejs');

// top-level middlewares
app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(cors());
app.use(session({
    saveUninitialized: false,
    resave: false,
    secret: 'sdfigoi93458739ofghdkljhkglsdfkl',
    cookie: {
        maxAge: 1200000
    }
}));


app.use((req, res, next)=>{
    res.locals.myData = {
        name: 'david',
        age: 24
    }
    res.locals.name = 'Shinder Lin';
    res.locals.pageName = '';
    next();
});

app.get('/', (req, res)=>{
    res.render('home', {name: 'Bill'});
});

app.get('/json-sales', (req, res)=>{
    const data = require(__dirname + '/../data/sales');
    res.render('json-sales', {sales: data});
});

app.get('/try-qs', (req, res)=>{
    res.json(req.query);
});

app.post('/try-post', (req, res)=>{
    res.json(req.body);
});

app.get('/try-post-form', (req, res)=>{
    res.render('try-post-form');
});

app.post('/try-post-form', (req, res)=>{
    res.render('try-post-form', req.body);
});

app.post('/try-upload', upload.single('avatar'), (req, res)=>{
    // console.log(req.file);
    if(req.file && req.file.originalname){
        if(/\.(jpg|jpeg|png|gif)$/i.test(req.file.originalname) ){
            fs.rename(req.file.path, './public/img/'+req.file.originalname, error=>{
                res.json({success: true});
            });
        } else {
            fs.unlink(req.file.path, error=>{
                res.json({success: false, error: 'bad file type!'});
            });
        }
    } else {
        res.json({success: false, error: 'no upload file !'});
    }

});
app.post('/try-upload2', upload2.single('avatar'), (req, res)=>{
    res.json({
        file: req.file,
        body: req.body,
    });
});
app.post('/try-upload2a', upload2.array('photos', 10), (req, res)=>{
    res.json({
        files: req.files,
        body: req.body,
    });
});

app.get('/try-uuid', (req, res)=>{
    res.json({
        u1: uuidv4(),
        u2: uuidv4(),
    });
});

app.get('/my-params1/:action?/:id?', (req, res)=>{
    res.json(req.params);
});
app.get(/\/m\/09\d{2}-?\d{3}-?\d{3}$/i, (req, res)=>{
    //res.send(req.url);
    let u = req.url.slice(3);
    u = u.split('?')[0];
    u = u.split('-').join('');
    res.send(u);
});

app.use(require(__dirname+'/admins/admin2'));
app.use('/aaa', require(__dirname+'/admins/admin2'));

app.get('/try-session', (req, res)=>{
    req.session.myVar = req.session.myVar || 1;
    req.session.myVar++;
    res.json({
        'session.myVar': req.session.myVar,
        'session': req.session
    })
});
app.get('/try-moment', (req, res)=>{
    const fm = 'YYYY-MM-DD HH:mm:ss';
    const mo1 = moment(req.session.cookie.expires);
    const mo2 = moment(new Date());

    res.json({
        mo1: mo1.format(fm),
        mo1tz: mo1.tz('Europe/London').format(fm),
        mo2: mo2.format(fm),
        mo2tz: mo2.tz('Europe/London').format(fm),
        now: new Date()
    })

});

app.get('/try-db', async (req, res)=>{
    const sql = "SELECT * FROM address_book";

    const result = await db.myExecSql(sql);

    res.json(result);
});

app.use('/address-book', require(__dirname+'/address-book'));


// --- static folder ---
app.use( express.static('public'));
// --- 404 ---
app.use( (req, res)=>{
    res.status(404).send('<h2>404 - 找不到網頁</h2>')
});

app.listen(3000, ()=>{
    console.log('server started!');
});


