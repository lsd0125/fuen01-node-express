class Person {
    constructor(name=''){
        this.name = name;
    }
    toJSON() {
        return JSON.stringify({name: this.name});
    }
}

const f = a=>a*a;

module.exports = {
    Person,
    f
};