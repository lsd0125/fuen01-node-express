const express = require('express');

console.log('express.shinder_data:', express.shinder_data);

const router = express.Router();

router.get('/admin2/:p1?/:p2?', (req, res)=>{
    res.json({
        'req.params': req.params,
        'req.url': req.url,
        'req.baseUrl': req.baseUrl,
        'myData': res.locals.myData
    });
});

module.exports = router;




