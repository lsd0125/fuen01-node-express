CREATE TABLE address_book (
sid INT IDENTITY PRIMARY KEY,
name NVARCHAR(100),
email NVARCHAR(100),
mobile NVARCHAR(100),
birthday DATE,
address NVARCHAR(100),
created_at DATETIME
);